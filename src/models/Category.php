<?php

namespace quoma\modules\config\models;

use Yii;
use quoma\modules\config\ConfigModule;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "category".
 *
 * @property integer $category_id
 * @property string $name
 * @property string $status
 * @property boolean $superadmin
 * @property boolean $slug
 *
 * @property Item[] $items
 */
class Category extends \quoma\core\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_config');
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
                'ensureUnique' => true,
                'immutable' => true
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'string'],
            [['name'], 'string', 'max' => 45],
            [['superadmin'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => ConfigModule::t('Category ID'),
            'name' => ConfigModule::t('Name'),
            'status' => ConfigModule::t('Status'),
            'superadmin' => ConfigModule::t('Superadmin only'),
            'slug' => ConfigModule::t('Slug'),
        ];
    }    


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['category_id' => 'category_id']);
    }
         
    /**
     * @inheritdoc
     * Strong relations: None.
     */
    public function getDeletable()
    {
        return true;
    }
    
    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: Items.
     */
    protected function unlinkWeakRelations(){
    }
    
    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if($this->getDeletable()){
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Vuelve los valores de todas las configuraciones a sus valores por defecto.
     * Si el usuario no es superadmin, solo modifica las configuraciones que no tengan
     * el requisito de usuario superadmin.
     */
    public function resetValues()
    {
        
        $items = $this->getItems();
        
        if(!Yii::$app->user->isSuperadmin){
            $items->andWhere(['superadmin' => 0]);
        }
        
        foreach($items->all() as $item){
            Item::setValue($item->attr, $item->default, false);
        }
        
    }
    
}
