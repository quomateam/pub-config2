<?php

namespace quoma\modules\config\models;

use quoma\media\behaviors\MediaBehavior;
use quoma\media\models\ModelHasMedia;
use Yii;
use quoma\modules\config\ConfigModule;
use yii\web\Application;

/**
 * This is the model class for table "item".
 *
 * @property integer $item_id
 * @property string $attr
 * @property string $type
 * @property string $default
 * @property string $label
 * @property string $description
 * @property integer $multiple
 * @property integer $category_category_id
 *
 * @property Config[] $configs
 * @property Category $categoryCategory
 */
class Item extends \quoma\core\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_config');
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        if (Yii::$app instanceof Application) {

            return [
                'media' => [
                    'class' => MediaBehavior::class,
                    'modelClass' => static::class,
                ],
            ];
        } else {
            return [];
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attr', 'category_id'], 'required'],
            [['multiple', 'category_id'], 'integer'],
            [['label'], 'string', 'max' => 140],
            [['attr', 'type'], 'string', 'max' => 45],
            [['default', 'description', 'model', 'model_name_attr'], 'string', 'max' => 255],
            [['attr'], 'unique'],
            [['superadmin'], 'boolean'],
            [['model', 'model_name_attr'], 'required', 'when' => function($model) {
                return $model->type == 'dropdownModel';
            }],
            [['model'], function(){
                if(!class_exists($this->model)){
                    $this->addError('model', Yii::t('app', 'Class {model} does not exists.', ['model' => $this->model]));
                }
            }, 'when' => function($model) {
                return $model->type == 'dropdownModel';
            }],
            [['model_name_attr'], function(){
                if(empty($this->model) || !class_exists($this->model)){
                    return;
                }
                $aux = new $this->model;
                if(!$aux->hasProperty($this->model_name_attr) && !$aux->hasAttribute($this->model_name_attr)){
                    $this->addError('model_name_attr', Yii::t('app', 'Attribute {name} is not defined.', ['name' => $this->model_name_attr]));
                }
            }, 'when' => function($model) {
                return $model->type == 'dropdownModel';
            }]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_id' => ConfigModule::t('Item ID'),
            'attr' => ConfigModule::t('Attr'),
            'type' => ConfigModule::t('Type'),
            'default' => ConfigModule::t('Default'),
            'label' => ConfigModule::t('Label'),
            'description' => ConfigModule::t('Description'),
            'multiple' => ConfigModule::t('Multiple'),
            'category_id' => ConfigModule::t('Category'),
            'superadmin' => ConfigModule::t('Superadmin only'),
            'model' => ConfigModule::t('Model Class'),
            'model_name_attr' => ConfigModule::t('Model name attribute'),
        ];
    }    


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfigs()
    {
        return $this->hasMany(Config::className(), ['item_id' => 'item_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRules()
    {
        return $this->hasMany(Rule::className(), ['item_id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['category_id' => 'category_id']);
    }
             
    /**
     * @inheritdoc
     * Strong relations: None.
     */
    public function getDeletable()
    {
        return true;
    }
    
    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: Configs, CategoryCategory.
     */
    protected function unlinkWeakRelations(){
        
        $this->unlinkAll('configs', true);
        $this->unlinkAll('rules', true);
        
    }
    
    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if($this->getDeletable()){
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }
    
    public static function types()
    {
        return [
            'textInput' => Yii::t('app', 'Text'),
            'textarea' =>  Yii::t('app', 'Textarea'),
            'checkbox' => Yii::t('app','Checkbox'),
            'passwordInput' => Yii::t('app','Password'),
            'dropdownModel' => Yii::t('app','Dropdown Model'),
            'image' => Yii::t('app','Image'),
            'images' => Yii::t('app','Images'),
            'media' => Yii::t('app','Media'),
        ];
    }
    
    public static function getItems($category)
    {
        
        $query = Item::find();
        
        $query->where(['category_id' => $category->category_id]);
        
        if(!Yii::$app->user->isSuperadmin){
            $query->andWhere(['superadmin' => 0]);
        }
        
        return $query->all();
        
    }
    
    /**
     * Para tipo dropdownModel, busca los modelos
     * @return type
     */
    public function getModelDropdownItems()
    {
        $aux = new $this->model;
        
        $items = $aux::find()->all();
        
        $nameAttr = $this->model_name_attr;
        return \yii\helpers\ArrayHelper::map($items, $aux->primaryKey(), $nameAttr);
    }

    /**
     * @param $attr
     * @param $value
     * @param bool $validate
     * @return array|Config|\yii\db\ActiveRecord|null
     * @throws \yii\web\HttpException
     */
    public static function setValue($attr, $value, $validate = true) {

        $item = Item::find()->where(['attr' => $attr])->one();
        if($item === null){
            throw new \Exception('Configuration item not found: '.$attr);
        }

        if (!\Yii::$app->request->isConsoleRequest && $item->superadmin && !Yii::$app->user->isSuperadmin) {
            throw new \yii\web\HttpException(403, ConfigModule::t('Forbidden.'));
        }

        if($item->isMediaType){
            $item->saveMedia($value);
            return $item;
        }

        $config = Config::find()->where(['item_id' => $item->item_id])->one();

        if (empty($config)) {
            $config = new Config;
            $config->item_id = $item->item_id;
        }

        $config->value = $value;

        $config->save($validate);

        return $config;
    }

    /**
     * Setea los recursos multimedia al modelo
     * @param array $media
     * @throws \yii\web\HttpException
     */
    public function saveMedia($values){

        $key = $this->primaryKey;
        $class = static::class;

        //Quitamos las relaciones actuales
        ModelHasMedia::deleteAll(['model_id'=>$key, 'model'=>$class]);

        //Guardamos las nuevas relaciones
        foreach ($values as $order=>$m){
            $mhm = new ModelHasMedia();

            $mhm->media_id = $m;
            $mhm->model_id = $key;
            $mhm->model = $class;
            $mhm->order = $order;

            $mhm->save();
        }
    }

    public function getIsMediaType()
    {
        return in_array($this->type, ['media', 'image', 'images']);
    }

}
