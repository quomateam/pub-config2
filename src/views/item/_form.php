<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use quoma\modules\config\ConfigModule;

/* @var $this yii\web\View */
/* @var $model quoma\modules\config\models\Item */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'category_id')->dropDownList(yii\helpers\ArrayHelper::map(
        \quoma\modules\config\models\Category::find()->all(), 'category_id', 'name'
    )) ?>

    <?= $form->field($model, 'attr')->textInput(['maxlength' => 45]) ?>
    
    <?= $form->field($model, 'label')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'type')->dropDownList(quoma\modules\config\models\Item::types(), ['data-type' => true]) ?>
    
    <div data-show-on-type="dropdownModel" style="<?php if($model->type !== 'dropdownModel') echo 'display:none'; ?>">
        <?= $form->field($model, 'model', ['enableClientValidation' => false])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'model_name_attr', ['enableClientValidation' => false])->textInput(['maxlength' => true]) ?>
    </div>

    <?= $form->field($model, 'default')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 255]) ?>
    
    <?= $form->field($model, 'superadmin')->checkbox() ?>

    <?= $form->field($model, 'multiple')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? ConfigModule::t('Create') : ConfigModule::t('Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    var Item = new function()
    {
        this.init = function()
        {
            $('[data-type]').on('change', function(){
                var type = $(this).val();
                $('[data-show-on-type]').not('[data-show-on-type="'+type+'"]').hide();
                $('[data-show-on-type="'+type+'"]').show();
            });
            
        }
        
    }
</script>
<?php $this->registerJs('Item.init();') ?>